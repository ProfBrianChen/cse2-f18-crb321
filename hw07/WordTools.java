//Cole Buck     CSE 002     10/31/18     HW 7 Word Tools

import java.util.Scanner;

public class WordTools {
  
  public static void main(String[] args) {
   
    String userString = sampleText();
    System.out.print("\nYou entered: ");
    System.out.println(userString);
    
    String userChoice = "";
    
    //program will run until user chooses q
    while(userChoice.equals("q") == false) {
      
      userChoice = printMenu();
      
      //runs appropriate method according to letter picked 
      if(userChoice.equals("c") == true) {
        getNumOfNonWsCharacters(userString);
      } else if(userChoice.equals("w") == true) {
        getNumOfWords(userString);
      } else if(userChoice.equals("f") == true) {
        findText(userString);
      } else if(userChoice.equals("r") == true) {
        replaceExclamation(userString);
      } else if(userChoice.equals("s") == true) {
        shortenSpace(userString);
      } else if(userChoice.equals("q") == true) {
        System.out.println("Quitting");
      } else {
        System.out.println("Invalid choice. Please choose a letter from the menu.");
      }
    }
  }
  
  static void getNumOfNonWsCharacters(String s) {
      int count = 0;
      int length = s.length();
     
      for(int i = 0; i < length; ++i) {
        
        if(s.charAt(i) == ' ') {
          
        } else {
          ++count;
        }
      }
      
      System.out.print("Number of non-whitespace characters: ");
      System.out.println(count);
  }
    
  static void getNumOfWords(String s) {
    int count = 0;
    int length = s.length();
    boolean firstCharacter = true;
    
    for(int i = 0; i < length; ++i) {
      
      if(s.charAt(i) == ' ') {
        firstCharacter = true;
      } else if(firstCharacter == true) {
        ++count;
        firstCharacter = false;
      }
    }
    
    System.out.print("Number of words: ");
    System.out.println(count);
  }
  
  static void findText(String s) {
    int count = 0;
   
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter a word or phrase to be found (case-sensitive): ");
    
    while(myScanner.hasNextInt() == true || myScanner.hasNextDouble() == true) {
      System.out.println("Invalid input. Please enter a valid string: ");
      myScanner.next();
    }
    String search = myScanner.nextLine();

  
    for(int i = s.indexOf(search); i >= 0; i = s.indexOf(search, i + 1)){
      ++count;
    }    
    
    System.out.print("Instances of '");
    System.out.print(search);
    System.out.print("': ");
    System.out.println(count);
  }
  
  static void replaceExclamation(String s) {
    
    System.out.print("Edited text: ");
    System.out.println(s.replace('!','.'));
  }
  
  static void shortenSpace(String s) {
    
    s = s.replaceAll("\\s+", " ");
    
    System.out.print("Edited text: ");
    System.out.println(s);
  }
  
  static String printMenu() {
    
    Scanner myScanner = new Scanner(System.in);
    System.out.println("\nMENU \nc - Number of non-whitespace characters \nw - Number of words \nf - Find text \nr - Replace all !s \ns - Shorten spaces \nq - Quit \n\nChoose an option: ");
    
    while(myScanner.hasNextInt() == true || myScanner.hasNextDouble() == true) {
      System.out.println("Invalid input. Please enter a valid string from the menu: ");
      myScanner.next();
    }
    String answer = myScanner.next();
  
    
    return answer;
  }
  
  static String sampleText() {
    
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Please enter text: ");
    
    while(myScanner.hasNextInt() == true || myScanner.hasNextDouble() == true) {
      System.out.println("Invalid input. Please enter a valid string: ");
      myScanner.next();
    }
    String input = myScanner.nextLine();

    
    return input;
  }
}