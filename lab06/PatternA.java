//Cole Buck   10/11/18    CSE 002   Lab 06 Pattern A

import java.util.Scanner;

public class PatternA {

  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Please enter an integer between 1 and 10: ");
    
    while(myScanner.hasNextInt() == false) {
      
      System.out.println("Invalid input. Please enter an integer between 1 and 10: ");
      myScanner.next();
    }
       
    int length = myScanner.nextInt() + 1;
     
    for(int i = 0; i < length; ++i) {
      
      for(int j = 0; j < i; ++j) {
        
        System.out.print((j+1) + " ");
      }
      
      System.out.println("");
    }
  }
}