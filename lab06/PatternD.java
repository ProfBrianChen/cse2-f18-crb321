//Cole Buck   10/17/18    CSE 002   Lab 06 Pattern D

import java.util.Scanner;

public class PatternD {

  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Please enter an integer between 1 and 10: ");
    
    while(myScanner.hasNextInt() == false) {
      
      System.out.println("Invalid input. Please enter an integer between 1 and 10: ");
      myScanner.next();
    }
       
    int length = myScanner.nextInt();
     
    for(int i = length; i > 0; --i) {
      
      for(int j = length; j > 0; --j) {
        
        System.out.print((j) + " ");
      }
      --length;
      
      System.out.println("");
    }
  }
}