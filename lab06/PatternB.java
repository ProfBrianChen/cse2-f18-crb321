//Cole Buck   10/17/18    CSE 002   Lab 06 Pattern B

import java.util.Scanner;

public class PatternB {

  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Please enter an integer between 1 and 10: ");
    
    while(myScanner.hasNextInt() == false) {
      
      System.out.println("Invalid input. Please enter an integer between 1 and 10: ");
      myScanner.next();
    }
       
    int length = myScanner.nextInt();
     
    for(int i = length; i > 0; --i) {
      
      for(int j = 0; j < i; ++j) {
        
        System.out.print((j+1) + " ");
      }
      --length;
      
      System.out.println("");
    }
  }
}