//Cole Buck   10/17/18    CSE 002   Lab 06 Pattern C

import java.util.Scanner;

public class PatternC {

  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Please enter an integer between 1 and 10: ");
    
    while(myScanner.hasNextInt() == false) {
      
      System.out.println("Invalid input. Please enter an integer between 1 and 10: ");
      myScanner.next();
    }
       
    int length = myScanner.nextInt();
    int spaceLength = length; // amount of space to add before each line of numbers
     
    for(int i = 0; i < length; ++i) {
      
      //Adds space so that the numbers are alligned with the right side
      for(int k = 0; k < spaceLength; ++k) {
        System.out.print("  ");
      }
      
      for(int j = i; j>-1 ; --j) {
        System.out.print((j+1) + " ");
      }
      
      --spaceLength;
      System.out.println("");
    }
  }
}