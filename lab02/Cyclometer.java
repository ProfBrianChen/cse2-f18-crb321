//Cole Buck   9/6/18    CSE 002   Cyclometer
public class Cyclometer {
      // main method required for every Java program
    public static void main(String[] args) {
      
      int secsTrip1 = 480; //number of seconds of first trip
      int secsTrip2 = 3220; //number of seconds of second trip
      int countsTrip1 = 1561; //number of wheel rotations during first trip
      int countsTrip2 = 9037; //number of wheel roations during second trip
      
      double wheelDiameter = 27.0; //diameter of front wheel
      double PI = 3.14159; //pi value to five decimal places
      int feetPerMile = 5280; //number of feet in one mile
      int inchesPerFoot = 12; //number of inches in one foot
      double secondsPerMinute = 60; //number of seconds in one minute
      double distanceTrip1, distanceTrip2, totalDistance; //values to be calculated later
      
      System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
      System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
      
      //calculating and storing values
      distanceTrip1 = countsTrip1 * wheelDiameter * PI; //distance travelled in inches
      distanceTrip1 /= inchesPerFoot * feetPerMile; //distance travelled in miles
      
      distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
      totalDistance = distanceTrip1 + distanceTrip2;
      
      //print out data
      System.out.println("Trip 1 was " + distanceTrip1 + " miles");
      System.out.println("Trip 2 was " + distanceTrip2 + " miles");
      System.out.println("The total distance was " + totalDistance + " miles");
      
      
    }  //end of main method 
  
} //end of class