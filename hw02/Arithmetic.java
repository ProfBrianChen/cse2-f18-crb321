//Cole Buck   CSE 002   9/11/18   Arithmetic  

public class Arithmetic {
  
  public static void main(String[] args) {
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //Cost per belt
    double beltPrice = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //total costs of each item
    double totalCostPants = numPants * pantsPrice;
    double totalCostShirts = numShirts * shirtPrice;
    double totalCostBelts = numBelts * beltPrice;
    
    //total cost before tax
    double totalCostPreTax = totalCostPants + totalCostShirts + totalCostBelts;
    
    //sales tax for each item, * 100 is part of process for displaying two decimal places
    double salesTaxPants = totalCostPants * paSalesTax * 100;
    double salesTaxShirts = totalCostShirts * paSalesTax * 100;
    double salesTaxBelts = totalCostBelts * paSalesTax * 100;
    
    //process for displaying two decimal places
    int salesTaxPantsI = (int) salesTaxPants;
    double salesTaxPantsD = (double) salesTaxPantsI / 100.0;
    int salesTaxShirtsI = (int) salesTaxShirts;
    double salesTaxShirtsD = (double) salesTaxShirtsI / 100.0;
    int salesTaxBeltsI = (int) salesTaxBelts;
    double salesTaxBeltsD = (double) salesTaxBeltsI / 100.0;

        
    //total sales tax for everything
    double totalSalesTax = salesTaxPantsD + salesTaxShirtsD + salesTaxBeltsD;
    
    //total transaction cost
    double totalTransactionCost = totalCostPreTax + totalSalesTax;
    
    //display statements
    System.out.println("Total Cost of Pants: $" + totalCostPants + "   Sales Tax on Pants: $" + salesTaxPantsD);
    System.out.println("Total Cost of Shirts: $" + totalCostShirts + "   Sales Tax on Shirts: $" + salesTaxShirtsD);
    System.out.println("Total Cost of Belts: $" + totalCostBelts + "    Sales Tax on Belts: $" + salesTaxBeltsD);
    System.out.println("Total Cost Before Tax: $" + totalCostPreTax);
    System.out.println("Total Sales Tax: $" + totalSalesTax);
    System.out.println("Total Cost of the Purchase: $" + totalTransactionCost);
    
  }
}