//Cole Buck   CSE 002   9/13/18   Check
//Determine original cost of check, percentage tip, how the check can be split, and how much each person will pay

import java.util.Scanner;

public class Check { 

  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.print("Enter the original cost of the check in the form xx.xx: ");  
    double checkCost = myScanner.nextDouble();
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;  //converts to decimal
    
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars;  //whole dollar amount of cost
    int dimes;  //stores first digit to right of decimal point
    int pennies;  //stores second digit to right of decimal point
    
    totalCost = checkCost * (1+tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int) costPerPerson;  //gets amount of dollars
    dimes = (int) (costPerPerson * 10) % 10;  //gets amount of dimes
    pennies = (int) (costPerPerson * 100) % 10; //gets amount of pennies
    
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  }
}