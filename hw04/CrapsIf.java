//Cole Buck   9/26/18   CSE 002   HW04 Craps

import java.util.Scanner;
import java.util.Random;
  
public class CrapsIf {
  
  public static void main(String[] args) {
    
    Scanner scanner = new Scanner(System.in);
    Random randGen = new Random();
    int dice1 = 0;
    int dice2 = 0;
    String slangName = "";
    
    System.out.println("Enter '1' for randomly cast die, enter '2' to manually state two dice");
    int choice = scanner.nextInt();
    
    if(choice == 1) {
      
      dice1 = randGen.nextInt(6) + 1;
      dice2 = randGen.nextInt(6) + 1;
      
    } else if (choice ==2) {
      
      System.out.println("Enter first dice value, 1-6");
      dice1 = scanner.nextInt();
      
      if (dice1 < 1 || dice1 > 6) {
        System.out.println("Invalid dice value, defaulting to  1");
        dice1 = 1;
      }
      
      System.out.println("Enter  second dice value, 1-6");
      dice2 = scanner.nextInt();
      
      if (dice2 < 1 || dice2 > 6) {
        System.out.println("Invalid dice value, defaulting to 1");
        dice2 = 1;
      }
    }
    
    if (dice1 == 1) {
      
      if(dice2 == 1) {
        slangName = "Snake Eyes";
      } else if (dice2 == 2) {
        slangName = "Ace Deuce";
      } else if (dice2 == 3) {
        slangName = "Easy Four";
      } else if (dice2 == 4) {
        slangName = "Fever Five";
      } else if (dice2 == 5) {
        slangName = "Easy Six";
      } else if(dice2 == 6) {
        slangName = "Seven Out";
      }
      
    } else if (dice1 == 2) {
      
      if(dice2 == 1) {
        slangName = "Ace Deuce";
      } else if (dice2 == 2) {
        slangName = "Hard Four";
      } else if (dice2 == 3) {
        slangName = "Fever Five";
      } else if (dice2 == 4) {
        slangName = "Easy Six";
      } else if (dice2 == 5) {
        slangName = "Seven Out";
      } else if(dice2 == 6) {
        slangName = "Easy Eight";
      }
      
      
    } else if (dice1 == 3) {
      
      if(dice2 == 1) {
        slangName = "Easy Four";
      } else if (dice2 == 2) {
        slangName = "Fever Five";
      } else if (dice2 == 3) {
        slangName = "Hard Six";
      } else if (dice2 == 4) {
        slangName = "Seven Out";
      } else if (dice2 == 5) {
        slangName = "Easy Eight";
      } else if(dice2 == 6) {
        slangName = "Nine";
      }
      
    } else if (dice1 == 4) {
      
      if(dice2 == 1) {
        slangName = "Fever Five";
      } else if (dice2 == 2) {
        slangName = "Easy Six";
      } else if (dice2 == 3) {
        slangName = "Seven Out";
      } else if (dice2 == 4) {
        slangName = "Hard Eight";
      } else if (dice2 == 5) {
        slangName = "Nine";
      } else if(dice2 == 6) {
        slangName = "Easy Ten";
      }
      
    } else if (dice1 == 5) {
      
      if(dice2 == 1) {
        slangName = "Easy Six";
      } else if (dice2 == 2) {
        slangName = "Seven Out";
      } else if (dice2 == 3) {
        slangName = "Easy Eight";
      } else if (dice2 == 4) {
        slangName = "Nine";
      } else if (dice2 == 5) {
        slangName = "Hard Ten";
      } else if(dice2 == 6) {
        slangName = "Yo-leven";
      }
      
    } else if (dice1 == 6) {
      
      if(dice2 == 1) {
        slangName = "Seven Out";
      } else if (dice2 == 2) {
        slangName = "Easy Eight";
      } else if (dice2 == 3) {
        slangName = "Nine";
      } else if (dice2 == 4) {
        slangName = "Easy Ten";
      } else if (dice2 == 5) {
        slangName = "Yo-leven";
      } else if(dice2 == 6) {
        slangName = "Boxcars";
      }
    }
    
    System.out.println(slangName);
  }
}