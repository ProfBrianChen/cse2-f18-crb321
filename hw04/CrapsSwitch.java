//Cole Buck   9/26/18   CSE 002   HW04 Craps

import java.util.Scanner;
import java.util.Random;

public class CrapsSwitch {
  
  public static void main(String[] args) {
    
    Scanner scanner = new Scanner(System.in);
    Random randGen = new Random();
    int dice1 = 0;
    int dice2 = 0;
    String slangName = "";
    
    System.out.println("Enter '1' for randomly cast die, enter '2' to manually state two dice");
    int choice = scanner.nextInt();
    
    switch (choice) {
        
      case 1: 
        dice1 = randGen.nextInt(6) + 1;
        dice2 = randGen.nextInt(6) + 1;
        break;
       
      case 2:
        System.out.println("Enter first dice value, 1-6");
        dice1 = scanner.nextInt();
        
        System.out.println("Enter  second dice value, 1-6");
        dice2 = scanner.nextInt();
        break;
    }
    
    
    switch (dice1) {
        
      case 1:
        switch (dice2) {
          case 1:
            slangName = "Snake Eyes";
            break;
          case 2:
            slangName = "Ace Deuce";
            break;
          case 3:
            slangName = "Easy Four";
            break;
          case 4:
            slangName = "Fever Five";
            break;
          case 5:
            slangName = "Easy Six";
            break;
          case 6:
            slangName = "Seven Out";
            break;
        }
        break;
      case 2:
        switch (dice2) {
          case 1:
            slangName = "Ace Deuce";
            break;
          case 2:
            slangName = "Hard Four";
            break;
          case 3:
            slangName = "Fever Five";
            break;
          case 4:
            slangName = "Easy Six";
            break;
          case 5:
            slangName = "Seven Out";
            break;
          case 6:
            slangName = "Easy Eight";
            break;
        }
        break;
      case 3:
        switch (dice2) {
          case 1:
             slangName = "Easy Four";
            break;
          case 2:
             slangName = "Fever Five";
            break;
          case 3:
             slangName = "Hard Six";
            break;
          case 4:
             slangName = "Seven Out";
            break;
          case 5:
             slangName = "Easy Eight";
            break;
          case 6:
             slangName = "Nine";
            break;
        }
        break;
      case 4:
        switch (dice2) {
          case 1:
             slangName = "Fever Five";
            break;
          case 2:
             slangName = "Easy Six";
            break;
          case 3:
             slangName = "Seven Out";
            break;
          case 4:
             slangName = "Hard Eight";
            break;
          case 5:
             slangName = "Nine";
            break;
          case 6:
             slangName = "Easy Ten";
            break;
        }
        break; 
      case 5:
        switch (dice2) {
          case 1:
             slangName = "Easy Six";
            break;
          case 2:
            slangName = "Seven Out";
            break;
          case 3:
            slangName = "Easy Eight";
            break;
          case 4:
            slangName = "Nine";
            break;
          case 5:
            slangName = "Hard Ten";
            break;
          case 6:
            slangName = "Yo-leven";
            break;
        }
        break;
      case 6:
        switch (dice2) {
          case 1:
            slangName = "Seven Out";
            break;
          case 2:
            slangName = "Easy Eight";
            break;
          case 3:
            slangName = "Nine";
            break;
          case 4:
            slangName = "Easy Ten";
            break;
          case 5:
            slangName = "Yo-leven";
            break;
          case 6:
            slangName = "Boxcars";
            break;
        }
        break; 
    }
    
    System.out.println(slangName);
    
  }
}