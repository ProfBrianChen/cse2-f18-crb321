//Cole Buck     CSE 002     12/4/18     HW10 Tic-Tac-Toe

import java.util.Scanner;

public class HW10 {

  public static void main(String[] args) {
  
    String[][] board = { {"1", "2", "3"}, {"4", "5", "6"}, {"7", "8", "9"},};
    boolean oTurn = true;
    boolean gameWon = false;
    boolean allSpacesTaken = false;
    
    //plays until there are no spaces left or there is a winner
    while (allSpacesTaken == false && gameWon == false) {
      
      //prints board
      printBoard(board);
      //announces who's turn it is
      if(oTurn == true) {
        System.out.println("It is O's turn.");
      } else {
        System.out.println("It is X's turn.");
      }
      
      //this method handles the process of making a move
      board = move(board, oTurn);
      
      //Determines if all of the spaces are filled with X's or O's
      allSpacesTaken = true;
      for(int i = 0; i < 3; ++i) {
        
        for(int j = 0; j < 3; ++j) {
          if(board[i][j] == "X" || board[i][j] == "O") {
          } else {
            allSpacesTaken = false;
          }
        }
      }
      
      //manually checks for each winning scenario
      if((board[0][0] == "X" && board[0][1] == "X" && board[0][2] == "X") || (board[0][0] == "O" && board[0][1] == "O" && board[0][2] == "O")) {
        gameWon = true;
      } else if ((board[1][0] == "X" && board[1][1] == "X" && board[1][2] == "X") || (board[1][0] == "O" && board[1][1] == "O" && board[1][2] == "O")) {
        gameWon = true;
      } else if ((board[2][0] == "X" && board[2][1] == "X" && board[2][2] == "X") || (board[2][0] == "O" && board[2][1] == "O" && board[2][2] == "O")) {
        gameWon = true;
      } else if ((board[0][0] == "X" && board[1][0] == "X" && board[2][0] == "X") || (board[0][0] == "O" && board[1][0] == "O" && board[2][0] == "O")) {
        gameWon = true;
      } else if ((board[0][1] == "X" && board[1][1] == "X" && board[2][1] == "X") || (board[0][1] == "O" && board[1][1] == "O" && board[2][1] == "O")) {
        gameWon = true;
      } else if ((board[0][2] == "X" && board[1][2] == "X" && board[2][2] == "X") || (board[0][2] == "O" && board[1][2] == "O" && board[2][2] == "O")) {
        gameWon = true;
      } else if ((board[0][0] == "X" && board[1][1] == "X" && board[2][2] == "X") || (board[0][0] == "O" && board[1][1] == "O" && board[2][2] == "O")) {
        gameWon = true;
      } else if ((board[0][2] == "X" && board[1][1] == "X" && board[2][0] == "X") || (board[0][2] == "O" && board[1][1] == "O" && board[2][0] == "O")) {
        gameWon = true;
      } 
      
      //turns alternate between X's and O's
      oTurn = !oTurn;
    }
    
    //announces end game scenarios
    if(gameWon == true) {
      if(oTurn == true) {
        System.out.println("Game over. X's won!");                       
      } else {
        System.out.println("Game over. O's won!");
      }
    } else if(allSpacesTaken == true) {
      System.out.println("Game over. All spaces are filled!");
    }
    
    //prints final board
    printBoard(board);
  }
  
  //this method handles the moving process
  public static String[][] move(String[][] board, boolean oTurn) {
    
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter integer corresponding to desired space");
    
    //checks if input is an int
    while(myScanner.hasNextInt() == false) {
      System.out.println("Invalid input, please enter an integer.");
      myScanner.next();
    }
    String space = myScanner.next();
    int spaceNum = Integer.valueOf(space);
    
    //checks if input is a valid space on the board
    if (spaceNum < 1 || spaceNum > 9) {
      System.out.print("Invalid input. ");
      board = move(board, oTurn);
    }  else {
      //checks if space is not already taken
      boolean openSpace = false;
      for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 3; ++j) {
          if(board[i][j].equals(space)) {
            openSpace = true;
          }
        }
      }
      if(openSpace == false) {
        System.out.print("Space is already taken. ");
        board = move(board, oTurn);
      }
      
      //places X or O onto the board
      for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
          if(board[i][j].equals(space)) {
            if(oTurn == true){
              board[i][j] = "O";
            } else {
              board[i][j] = "X";
            }
          }
        }
      }
    }
    return board;
  }
  
  //prints out the current board
  public static void printBoard(String[][] board) {
    
    for (int i = 0; i < 3; ++i) {
      
      for (int j = 0; j < 3; ++j) {
        System.out.print(board[i][j] + " ");
      }
      
      System.out.println("");
    }
  }
}