//Cole Buck    CSE 002     11/15/18     HW 8 Shuffling

import java.util.Scanner;
import java.util.Random;

public class hw08 { 
  
  public static void main(String[] args) { 
  
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
    } 
    System.out.println();
    printArray(cards); 
    shuffle(cards); 
    printArray(cards); 
    while(again == 1){ 
      
      //generates new deck if there aren't enough cards to create hand
      if(numCards > (index+1)){
        for (int i=0; i<52; i++){ 
          cards[i]=rankNames[i%13]+suitNames[i/13]; 
        }
        System.out.println("Generating new deck");
        printArray(cards); 
        shuffle(cards); 
        printArray(cards); 
        index = 51;
      }
      
      hand = getHand(cards,index,numCards); 
      System.out.println("Hand");
      printArray(hand);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
    scan.close();
  }
  
//shuffles deck
  public static void shuffle(String[] cards) {
    
    Random randGen = new Random();
    int randomIndex;
    String swap;
    
    for(int i = 0; i < 100; ++i){
      randomIndex = randGen.nextInt(51) + 1;
      swap = cards[0];
      cards[0] = cards[randomIndex];
      cards[randomIndex] = swap;
    }
    System.out.println("Shuffled");
  }
  
//prints out array 
  public static void printArray(String[] cards) {
    
    for(int i = 0; i < cards.length; ++i) {
      System.out.print(cards[i] + " ");
    }
    System.out.println();
  }
  
//generates hand from remaining deck
  public static String[] getHand(String[] cards, int index, int numCards) {
    String[] hand = new String[numCards];
    
    for(int i = 0; i < numCards; ++i) {
      hand[i] = cards[index - i];
    }
    
    return hand;
  }
}