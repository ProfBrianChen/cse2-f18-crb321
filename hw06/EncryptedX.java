//Cole Buck     CSE 002     10/24/18     HW 06 Encrypted X

import java.util.Scanner;

public class EncryptedX {
  
  public static void main(String[] args) {
    
    //size of x pattern
    int size = 0; 
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Please enter an integer between 0 and 100: ");
    
    //validates input
    while(myScanner.hasNextInt() == false) {
      
      System.out.println("Invalid input. Please enter an integer between 0 and 100: ");
      myScanner.next();
    }
    
    //added 1 so that pattern matches the size 10 pattern displayed in the homework prompt
    size = myScanner.nextInt() + 1;
    myScanner.close();
    
    //loop to create each row
    for(int i = 0; i < size; ++i) {
      
      //loop to create each column
      for(int j = 0; j < size; ++j) {
        
        //inserts spaces, same distance from left side and right side of pattern
        if(j==i || j==(size-i-1)){
          
          System.out.print(" ");
        } 
        //inserts stars
        else {
          
        System.out.print("*");
        }
      }
      
     //moves onto next row
     System.out.println(""); 
    }
  }
}