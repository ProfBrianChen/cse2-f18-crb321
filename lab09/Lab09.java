//Cole Buck     CSE 002     11/28/18     Lab 9

public class Lab09 {
  
  public static void main(String[] args) {
    
    int[] array0 = new int[] {2,3,5,8,4,7,1,9};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
    inverter(array0);
    print(array0);
    
    inverter2(array1);
    print(array1);
    
    int[] array3 = inverter2(array2);
    print(array3);
  }
  
  //creates copy of array
  public static int[] copy(int[] original) {
    
    int[] copy = new int[original.length];
    
    for (int i = 0; i < copy.length; ++i) {
      copy[i] = original[i];
    }
    return copy;
  }
  
  public static void inverter(int[] array) {
    
    int value;
    
    //inverts array elements
    for (int i = 0; i <= ((array.length - 1) / 2); ++i) {
      value = array[i];
      array[i] = array[array.length - 1 - i];
      array[array.length - 1 - i] = value;
    }
  }
  
  public static int[] inverter2(int[] array) {
    
    //creates copy of array
    int[] copy = copy(array); 
    
    int value;
    
    //inverts array elements
    for (int i = 0; i <= ((copy.length - 1) / 2); ++i) {
      value = copy[i];
      copy[i] = copy[copy.length - 1 - i];
      copy[copy.length - 1 - i] = value;
    }
    return copy;
  }
  
  //prints array
  public static void print(int[] array) {
  
    for (int i = 0; i < array.length; ++i) {
      System.out.print(array[i] + " ");
    }
    System.out.println("");
  }
}