//Cole Buck   9/20/18   CSE 002   Lab 04 Card Generator

import java.util.Random;

public class CardGenerator {
  
  public static void main(String[] args) {

    //default strings
    String suit = "";
    String cardId = "";
    
    //initializes random number generator
    Random randGen = new Random();
    
    //generates random number
    int cardNum = randGen.nextInt(52) + 1;
    
    //determines suit based on range
    if(cardNum < 14) {
      suit = "Diamonds";
    } else if (cardNum < 27) {
      suit = "Clubs";
      cardNum = cardNum - 13;
    }else if (cardNum < 40) {
      suit = "Hearts";
      cardNum = cardNum - 26;
    } else if (cardNum < 53) {
      suit = "Spades";
      cardNum = cardNum - 39;
    }
    
    //determines identity of card, regardless of suit
    switch(cardNum) {
      case 1:
        cardId = "Ace";
        break;
      case 2:
        cardId = "2";
        break;  
      case 3:
        cardId = "3";
        break;
      case 4:
        cardId = "4";
        break;
      case 5:
        cardId = "5";
        break;
      case 6:
        cardId = "6";
        break;  
      case 7:
        cardId = "7";
        break;
      case 8:
        cardId = "8";
        break;  
      case 9:
        cardId = "9";
        break;
      case 10:
        cardId = "10";
        break;
      case 11:
        cardId = "Jack";
        break;
      case 12:
        cardId = "Queen";
        break;    
      case 13:
        cardId = "King";
        break;
    }

    System.out.println("You picked the " + cardId + " of " + suit);
  }
}