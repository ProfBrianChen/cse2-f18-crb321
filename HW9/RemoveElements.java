//Cole Buck     CSE 002     11/28/18     HW 9

import java.util.Scanner;
import java.util.Random;

public class RemoveElements{
  
  public static void main(String [] arg){
    
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
 
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
    
    scan.close();
  }
  
  public static String listArray(int num[]){
    
    String out="{";
    
    for(int j=0;j<num.length;j++){
      
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    
    out+="} ";
    return out;
  }
  
  public static int[] randomInput() {
    
    int[] array = new int[10];
    Random randGen = new Random();
    
    //assigns random integer to each index
    for (int i = 0; i < 10; ++i) {
      array[i] = randGen.nextInt(10);
    } 
    return array;
  }
  
  public static int[] delete(int[] list, int pos) { 
    
    //creates new array with one less index
    int[] shortened = new int[list.length-1];
    
    if(pos < list.length) {
      
      //copies values from original array to new array, overwrites the pos index and shifts the remaining values down one index
      for (int i = 0; i < (list.length-1); ++i) {  
      
        if (i < pos) {
          shortened[i] = list[i];
        } else {
          shortened[i] = list[i+1];
        }
      }
      return shortened;
    } else {
      System.out.println("Index was out of bounds.");
      return list;
    }
  }
  
  public static int[] remove(int[] list, int target) {
    
    //number of times the target appears in the array
    int count = 0;
    
    //counts the number of times the target appears in the array
    for (int i = 0; i < list.length; ++i) {
      if (list[i] == target) {
        ++count;
      }
    }
    
    //creates a new array, accounting for removed elements
    int[] shortened = new int[list.length - count];
    int ahead = 0;
    
    //copies the values from the original array to the new array, overwrites the target elements, and shifts the remaining values down to a lower index
    for (int i = 0; i < list.length - count; ++i) {
      if (list[i + ahead] == target) {
        ++ahead;    
      } 
      shortened[i] = list[i + ahead];
      while(shortened[i] == target){
        ++ahead;
        shortened[i] = list[i + ahead];
      }
    }
    return shortened;
  }
}
