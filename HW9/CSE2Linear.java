//Cole Buck     CSE 002     11/28/18     HW 9

import  java.util.Scanner;
import java.util.Random;

public class CSE2Linear { 

  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    int[] grades = new int[15];
    
    for(int i = 0; i < 15; ++i) {
    
      System.out.println("Please enter grade #" + (i+1) + ":");
      
      //checks that input is an int
      while(myScanner.hasNextInt() == false){
        System.out.println("Invalid input. Please enter an int.");
        myScanner.next();
      }
      
      grades[i] = myScanner.nextInt();
    
      //checks that input is within the range 0-100
      if(grades[i] < 0 || grades[i] > 100) {
        System.out.println("Invalid input. Please enter an int between 0 and 100.");
        --i;
      }
      
      //checks that input is greater than or equal to previous grade
      if(i > 0) {
        if(grades[i] < grades[i-1]) {
          System.out.println("Invalid input. Grade must be higher than or equal to previous grade.");
          --i;
        }
      }
    }
    
    //prints array
    for(int i = 0; i < 15; ++i) {
      System.out.print(grades[i] + " ");
    }
    
    System.out.println("");
    System.out.println("Enter a grade to search for: ");
    int searchGrade = myScanner.nextInt();
    
    //searches for grade and scrambles array
    binarySearch(grades, searchGrade);
    grades = scramble(grades);
    
    System.out.println("Enter a grade to search for: ");
    searchGrade = myScanner.nextInt();
    
    //linear search for grade
    linearSearch(grades, searchGrade);
    
    myScanner.close();
  }
  
  //binary search for grade
  public static void binarySearch(int[] gradesList, int grade) {
    int low = 0;
    int high = 14;
    int index = -1;
    int count = 0;
    
    //binary search algorithm
    while(low <= high && index == -1) {
      int mid = (low + high) / 2;
      if (gradesList[mid] < grade) {
        low = mid + 1;
      } else if (gradesList[mid] > grade) {
        high = mid - 1;
      } else if (gradesList[mid] == grade) {
        index = mid;
      }
      ++count;
    }
    
    if(index == -1) {
      System.out.println(grade + " was not found in the list with " + count + " iterations.");
    } else {
      System.out.println(grade + " was found in the list with " + count + " iterations.");
    }
  }
  
  //scrambles array
  public static int[] scramble(int[]gradesList) {
    
    Random randGen = new Random();
    int value = 0;
    int index = randGen.nextInt(14) + 1;
    
    //swaps values around within array
    for(int i = 0; i < 100; ++i) {
      index = randGen.nextInt(14) + 1;
      value = gradesList[index];
      gradesList[index] = gradesList[0];
      gradesList[0] = value;
    }

    System.out.println("Scrambled: ");
    for(int i = 0; i < 15; ++i) {
      System.out.print(gradesList[i] + " ");
    }
    System.out.println("");  
    
    return gradesList;
  }
  
  public static void linearSearch(int[]gradesList, int grade) {
      
    int count = 0;
    int i = 0;
    int index = -1;
    
    while(index == -1 && i < 15) {
      
      if(gradesList[i] == grade) {
        index = i;
      }
      ++count;
      ++i;
    }
    
    if(index == -1) {
      System.out.println(grade + " was not found in the list with " + count + " iterations.");
    } else {
      System.out.println(grade + " was found in the list with " + count + " iterations.");
    }
  }
}