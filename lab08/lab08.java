//Cole Buck     CSE 002     11/14/18     Lab 08 Arrays

import java.util.Random;

public class lab08 {
  
  public static void main(String[] args) {
  
    //initializes arrays
    int[] array1 = new int[100];
    int[] array2 = new int[100];
    
    Random randGen = new Random();
    
    //assigns random integers to array 1
    for (int i = 0; i < 100; ++i) {
      array1[i] = randGen.nextInt(100);
    }
    
    //tracks occurrences of each integer
    for (int i = 0; i < 100; ++i) {
      array2[array1[i]] = array2[array1[i]] + 1;
    }
    
    System.out.println("Array 1 holds the following integers: ");
    
    //prints out list of integers in array 1
    for (int i = 0; i < 100; ++i) {
      System.out.print(array1[i] + " ");
    }
    
    System.out.println("\n");
    
    //prints out occurences of each integer
    for(int i = 0; i < 100; ++i) {
      
      if (array2[i] == 1) {
      System.out.println(i + " occurs " + array2[i] + " time");
      } else {
      System.out.println(i + " occurs " + array2[i] + " times");
      }      
    }    
  } 
}


