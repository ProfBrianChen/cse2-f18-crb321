//Cole Buck   10/4/18   CSE 002   Lab 5 Loops

import java.util.Scanner;

public class Loops {
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
    //Gets valid course number
    System.out.println("Please enter course number: ");
    
    while(myScanner.hasNextInt() == false) {
      System.out.println("Please enter an integer: ");
      myScanner.next();
    }
    int courseNumber = myScanner.nextInt();
    
    //Gets valid department name
    System.out.println("\nPlease enter department name: ");
    
    while(myScanner.hasNextInt() == true || myScanner.hasNextDouble() == true) {
      System.out.println("Please enter a string: ");
      myScanner.next();
    }    
    String departmentName = myScanner.next();
    
    //Gets valid number of times class meets
    System.out.println("\nPlease enter number of times the class meets per week: ");
    
    while(myScanner.hasNextInt() == false) {
      System.out.println("Please enter an interger: ");
      myScanner.next();
    }
    int meetingsPerWeek = myScanner.nextInt();
    
    //Gets valid start time of class
    System.out.println("\nPlease enter time the class starts: ");
    
    while(myScanner.hasNextInt() == false) {
      System.out.println("Please enter time as an integer: ");
      myScanner.next();
    }
    int startTime = myScanner.nextInt();
    
    //Gets valid instructor name
    System.out.println("\nPlease enter instructor's name: ");
    
    while(myScanner.hasNextInt() == true || myScanner.hasNextDouble() == true) {
      System.out.println("Please enter a string: ");
      myScanner.next();
    }
    String instructorName = myScanner.next();
    
    //Gets valid number of students in class
    System.out.println("\nPlease enter number of students in class: ");
    
    while(myScanner.hasNextInt() == false) {
      System.out.println("Please enter an integer: ");
      myScanner.next();
    }
    int numberStudents = myScanner.nextInt();
 
  }
}