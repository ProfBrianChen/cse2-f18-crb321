//Cole Buck     CSE 002     11/6/18     Lab 7 Methods

import java.util.Scanner;
import java.util.Random;

public class Methods {
  
  //used to save subject and keep it consistent in every sentence
  public static String subject;
  
  public static void main(String[] args) {
    
    //stores the user's input, used to determine when to quit program
    String choice = "";
    Scanner myScanner = new Scanner(System.in);
    
    while(choice.equals("q") == false) {
      
      //these three methods form the entire paragraph
      Thesis();  
      Action();
      Conclusion();
      System.out.println("Would you like another paragraph? If so, enter anything. Enter 'q' to quit.");
      choice = myScanner.next();
    }
    myScanner.close();
  }
  
  //creates thesis sentence
  public static void Thesis() {
    
    subject = Subject();
    System.out.println("The " + Adjective() + " " + Adjective() + " " + subject + " " + Verb() + " the " + Adjective() + " " + Object() + ".");
  }
  
  //creates action sentences
  public static void Action() {
    
    Random  randGen = new Random();
    int i = randGen.nextInt(3);
    
    System.out.println("This " + subject + " was " + Adjective() + " to the " + Adjective() + " " + Object() + ".");
    
    //generates a random number of action statements
    for(int c = 0; c <= i; ++c) {
      System.out.println("It " + Verb() + " a " + Object() + ".");
    }
  }
  
  //creates conclusion sentence
  public static void Conclusion() {
    
    System.out.println("That " + subject + " " + Verb() + " their " + Object() + "s.");
  }
  
  //generates random adjective
  public static String Adjective() {
    
    String s;
    Random  randGen = new Random();
    int i = randGen.nextInt(10);
    
    switch (i) {
      case 0:
        s = "quick";
        break;
      case 1:
        s = "slow";
        break;
      case 2:
        s = "large";
        break;
      case 3:
        s = "quiet";
        break;
      case 4:
        s = "green";
        break;
      case 5:
        s = "lazy";
        break;
      case 6:
        s = "round";
        break;
      case 7:
        s = "smart";
        break;
      case 8:
        s = "obnoxious";
        break;
      case 9:
        s = "boring";
        break;
      default:
        s = "error";
        break;
    }
    return s;
  }
  
  //generates random subject noun
  public static String Subject() {
    
    String s;
    Random  randGen = new Random();
    int i = randGen.nextInt(10);
    
    switch (i) {
      case 0:
        s = "fox";
        break;
      case 1:
        s = "farmer";
        break;
      case 2:
        s = "runner";
        break;
      case 3:
        s = "boy";
        break;
      case 4:
        s = "worker";
        break;
      case 5:
        s = "dog";
        break;
      case 6:
        s = "lizard";
        break;
      case 7:
        s = "professor";
        break;
      case 8:
        s = "mascot";
        break;
      case 9:
        s = "driver";
        break;
      default:
        s = "error";
        break;
    }
    return s;
  }
  
  //generates random past-tense verb
  public static String Verb() {
    
    String s;
    Random  randGen = new Random();
    int i = randGen.nextInt(10);
    
    switch (i) {
      case 0:
        s = "kicked";
        break;
      case 1:
        s = "broke";
        break;
      case 2:
        s = "passed";
        break;
      case 3:
        s = "grabbed";
        break;
      case 4:
        s = "stole";
        break;
      case 5:
        s = "found";
        break;
      case 6:
        s = "imagined";
        break;
      case 7:
        s = "forgot";
        break;
      case 8:
        s = "loved";
        break;
      case 9:
        s = "helped";
        break;
      default:
        s = "error";
        break;
    }
    return s;
  }
  
  //generates random object noun
  public static String Object() {
    
    String s;
    Random  randGen = new Random();
    int i = randGen.nextInt(10);
    
    switch (i) {
      case 0:
        s = "treasure";
        break;
      case 1:
        s = "cat";
        break;
      case 2:
        s = "rock";
        break;
      case 3:
        s = "football";
        break;
      case 4:
        s = "keys";
        break;
      case 5:
        s = "food";
        break;
      case 6:
        s = "guitar";
        break;
      case 7:
        s = "4 o'clock exam";
        break;
      case 8:
        s = "car";
        break;
      case 9:
        s = "door";
        break;
      default:
        s = "error";
        break;
    }
    return s;
  }
}