//Cole Buck   9/18/18   CSE 002   Pyramid

import java.util.Scanner;

public class Pyramid {
  
  public static void main(String[] args) {
    
    //Initializes scanner
    Scanner scanner = new Scanner(System.in);
    
    //Gets length input
    System.out.print("The square side of the pyramid is (input length): ");
    double length = scanner.nextDouble();
    
    //Gets height input
    System.out.print("The height of the pyramid is (input height): ");
    double height = scanner.nextDouble();
    
    //Calculates volume using length and height
    double volume = ((length * length) * height) / 3;
    
    //Displays volume
    System.out.println("The volume inside the pyramid is: " + volume);
  
  }
}