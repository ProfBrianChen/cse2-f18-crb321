//Cole Buck   9/18/18   CSE 002   Convert

import java.util.Scanner;

public class Convert {
  
  public static void main(String[] args) {
    
    //Initializes scanner
    Scanner scanner = new Scanner(System.in);
    
    //Gets acres input
    System.out.print("Enter the affected area in acres: ");
    double acres = scanner.nextDouble();
    
    //Gets rainfall input
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfall = scanner.nextDouble();
    
    //Calculates number of gallons and converts it to cubic miles, using conversion factors found online
    double gallons = acres * rainfall * 27154;
    double cubicMiles = gallons * (9.08 * Math.pow(10,-13));
    
    //Displays number of cubic  miles
    System.out.println(cubicMiles + " cubic miles");
    
  }
}