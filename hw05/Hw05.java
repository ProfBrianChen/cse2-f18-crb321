//Cole Buck   10/10/18    CSE 002   HW 05

import java.util.Scanner;
import java.util.Random;

public class Hw05 {
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    Random randGen = new Random();
   
    System.out.println("Enter number of hands to generate: ");
    
    while(myScanner.hasNextInt() == false) {
      System.out.println("Please enter an integer: ");
      myScanner.next();
    }
    int numHands = myScanner.nextInt();
       
    int card1 = 0;
    int card2 = 0;
    int card3 = 0;
    int card4 = 0;
    int card5 = 0;
    
    int value1 = 0;
    int value2 = 0;
    int value3 = 0;
    int value4 = 0;
    int value5 = 0;
    
    int occur1 = 0;
    int occur2 = 0;
    int occur3 = 0;
    int occur4 = 0;
    int occur5 = 0;
    int occur6 = 0;
    int occur7 = 0;
    int occur8 = 0;
    int occur9 = 0;
    int occur10 = 0;
    int occur11 = 0;
    int occur12 = 0;
    int occur13 = 0;
    
    int fourKind = 0;
    int threeKind = 0;
    int twoPair = 0;
    int onePair = 0;
    
    int count = 0;
    
    while(count < numHands) {
      
      card1 = randGen.nextInt(52) + 1;
      card2 = randGen.nextInt(52) + 1;
      while(card2 == card1) {
        card2 = randGen.nextInt(52) + 1;
      }
      card3 = randGen.nextInt(52) + 1;
      while(card3 == card1 || card3 == card2) {
        card3 = randGen.nextInt(52) + 1;
      }
      card4 = randGen.nextInt(52) + 1;
      while(card4 == card1 || card4 == card2 || card4 == card3) {
        card4 = randGen.nextInt(52) + 1;
      }
      card5 = randGen.nextInt(52) + 1;
      while(card5 == card1 || card5 == card2 || card5 == card3 || card5 == card4) {
        card5 = randGen.nextInt(52) + 1;
      }
      
      value1 = card1 % 13;
      value2 = card2 % 13;
      value3 = card3 % 13;
      value4 = card4 % 13;
      value5 = card5 % 13;
      
      switch (value1) {
        case 0: 
          occur1++;
          break;
        case 1:
          occur2++;
          break;
        case 2:
          occur3++;
          break;
        case 3:
          occur4++;
          break;
        case 4:
          occur5++;
          break;
        case 5:
          occur6++;
          break;
        case 6:
          occur7++;
          break;
        case 7:
          occur8++;
          break;
        case 8:
          occur9++;
          break;
        case 9:
          occur10++;
          break;
        case 10:
          occur11++;
          break;
        case 11:
          occur12++;
          break;
        case 12:
          occur13++;
          break;
      }
      
      switch (value2) {
        case 0: 
          occur1++;
          break;
        case 1:
          occur2++;
          break;
        case 2:
          occur3++;
          break;
        case 3:
          occur4++;
          break;
        case 4:
          occur5++;
          break;
        case 5:
          occur6++;
          break;
        case 6:
          occur7++;
          break;
        case 7:
          occur8++;
          break;
        case 8:
          occur9++;
          break;
        case 9:
          occur10++;
          break;
        case 10:
          occur11++;
          break;
        case 11:
          occur12++;
          break;
        case 12:
          occur13++;
          break;
      }
      
      switch (value3) {
        case 0: 
          occur1++;
          break;
        case 1:
          occur2++;
          break;
        case 2:
          occur3++;
          break;
        case 3:
          occur4++;
          break;
        case 4:
          occur5++;
          break;
        case 5:
          occur6++;
          break;
        case 6:
          occur7++;
          break;
        case 7:
          occur8++;
          break;
        case 8:
          occur9++;
          break;
        case 9:
          occur10++;
          break;
        case 10:
          occur11++;
          break;
        case 11:
          occur12++;
          break;
        case 12:
          occur13++;
          break;
      }
      
      switch (value4) {
        case 0: 
          occur1++;
          break;
        case 1:
          occur2++;
          break;
        case 2:
          occur3++;
          break;
        case 3:
          occur4++;
          break;
        case 4:
          occur5++;
          break;
        case 5:
          occur6++;
          break;
        case 6:
          occur7++;
          break;
        case 7:
          occur8++;
          break;
        case 8:
          occur9++;
          break;
        case 9:
          occur10++;
          break;
        case 10:
          occur11++;
          break;
        case 11:
          occur12++;
          break;
        case 12:
          occur13++;
          break;
      }
      
      switch (value5) {
        case 0: 
          occur1++;
          break;
        case 1:
          occur2++;
          break;
        case 2:
          occur3++;
          break;
        case 3:
          occur4++;
          break;
        case 4:
          occur5++;
          break;
        case 5:
          occur6++;
          break;
        case 6:
          occur7++;
          break;
        case 7:
          occur8++;
          break;
        case 8:
          occur9++;
          break;
        case 9:
          occur10++;
          break;
        case 10:
          occur11++;
          break;
        case 11:
          occur12++;
          break;
        case 12:
          occur13++;
          break;
      }
      
        
      
      if(occur1 == 4 || occur2 == 4 || occur3 == 4 || occur4 == 4 || occur5 == 4 || occur6 == 4 || occur7 == 4 || occur8 == 4 || occur9 == 4 || occur10 == 4 || occur11 == 4 || occur12 == 4 || occur13 == 4){
        fourKind++;
        System.out.println("---------------");
      }
    
    occur1 = 0;
    occur2 = 0;
    occur3 = 0;
    occur4 = 0;
    occur5 = 0;
    occur6 = 0;
    occur7 = 0;
    occur8 = 0;
    occur9 = 0;
    occur10 = 0;
    occur11 = 0;
    occur12 = 0;
    occur13 = 0;
      
      ++count;
      System.out.println(card1 + " " + card2 + " " +  card3 + " " +  card4 + " " +  card5 + "||||||" + value1 + " " + value2 + " " + value3 + " " + value4 + " " + value5);
      }
    
    System.out.println(fourKind);
    }
  }